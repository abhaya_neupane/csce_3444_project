from flask import Flask, render_template, request, redirect,url_for,flash,session
from werkzeug.security import generate_password_hash, check_password_hash
from flaskwebgui import FlaskUI #get the FlaskUI class
from flask_sqlalchemy import SQLAlchemy
from tmdbv3api import TMDb, Movie

tmdb = TMDb()
tmdb.api_key = 'ca7667ffc5f2b78ca578aa28ad92cb59'
tmdb.debug = True
movie = Movie()

app = Flask(__name__, template_folder='./Templates') 
app.secret_key=b'\x9e\x9c\x13\xca\xef\x0cB\x82#t3\xa3\x19qU2'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

ui = FlaskUI(app)

#Database class to store user information
class user_info(db.Model):
    __tablename__ = 'users'

    #All user information columns 
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name= db.Column(db.String(100), nullable=False)
    last_name= db.Column(db.String(100), nullable=False)
    username= db.Column(db.String(50), unique= True, nullable=False)
    password= db.Column(db.String(300), nullable=False)
    secret_ans=db.Column(db.String(500), nullable=False)

    def __init__(self, first_name, last_name, username, password,secret_ans):
        self.first_name = first_name
        self.last_name = last_name  
        self.username = username
        self.password = password
        self.secret_ans = secret_ans

#Database class to store user's watchlist
class watchlist(db.Model):
    __tablename__ ='mylist'

    #All user watchlist columns 
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username=db.Column(db.String(50),nullable=False)
    movie_id=db.Column(db.Integer, nullable=False)
    movie_name=db.Column(db.String(100), nullable=False)
    movie_score=db.Column(db.Float(2),nullable=False)

    def __init__(self, username, movie_id, movie_name, movie_score):
        self.username = username
        self.movie_id = movie_id
        self.movie_name = movie_name
        self.movie_score = movie_score

#Database class to store user's searchlist
class searchresults(db.Model):
    __tablename__='searchlist'

    #All user searchlist columns 
    movie_id = db.Column(db.Integer,primary_key=True)
    movie_name=db.Column(db.String(100), nullable=False)
    overview=db.Column(db.String(1000), nullable=False)
    movie_score=db.Column(db.Float(2),nullable=False)

    def __init__(self, movie_id, movie_name, overview, movie_score):
        self.movie_id= movie_id
        self.movie_name = movie_name
        self.overview = overview
        self.movie_score=movie_score

#Homepage
@app.route('/', methods=['POST', 'GET'])
def index():
    return render_template('index.html')

#Function that enters new user information into our user_info table
@app.route('/newuser' ,methods = ['GET', 'POST'])
def newuser():
    #If user successfully enters all sign up information 
    if request.method =="POST":
        if request.form['password']==request.form['password_again']:
            #Generating password hash for user privacy
            hashed_password=generate_password_hash(request.form['password'])
            hashed_secret_ans=generate_password_hash(request.form['secret_ans'])
            new_user=user_info(request.form['f_name'], request.form['l_name'],request.form['user_name'], hashed_password,hashed_secret_ans)

            try:
                #Storing user information inside user_info table
                db.session.add(new_user)
                db.session.commit()
                return redirect(url_for('existinguser'))
            except:
                #If infomation couldn't be entered into table ask user to enter all information again
                return render_template('newuser.html', title = 'New User', wrong_cred=1)
        else:
            #This happens usually when passwords don't match
            return render_template('newuser.html', title = 'New User', wrong_cred=2)
        
    else:
        return render_template('newuser.html', title = 'New User')
    
#Function that checks if username and password matches with what's in db
@app.route('/existinguser', methods=['GET', 'POST'])
def existinguser():
    if request.method =="POST":
        username=request.form['Username']
        #Saving username for the session for later use
        session['username']= username
        password=request.form['Password']

        #If username exists in db
        user_exists  = user_info.query.filter_by(username=username).first()
        if (user_exists):
            bool_password=check_password_hash(user_exists.password,password)
            #If user entered password matches with the encrypted password in db
            if bool_password:
                return redirect(url_for('userlist'))
            else:
                #If password is incorrect
                return render_template('existinguser.html', wrong_cred=1)
        else:
            #If username doesn't exist in db
            return render_template('existinguser.html', wrong_cred=1)
        
    return render_template('existinguser.html', title = 'Sign In')

#Function that displays user's watchlist and lets user search and add a new movie
@app.route('/mylist', methods=['GET', 'POST'])
def userlist():
    if request.method == "POST":
        #If user searches for a movie 
        if request.form['submit']=='Search':
            #First cleaning the searchlist table we use to store search resuts temporarily
            db.session.query(searchresults).delete()
            movie_name=request.form['movie_name']
            #Searching for movie
            search= movie.search(movie_name)

            #If no search results
            if not search:
                return render_template('userlist.html' , watchlist=watchlist.query.filter(watchlist.username==session['username']), no_result=1)
            else:
                for result in search:
                    #Saving search results into searchresults table and displaying it to user
                    movie_details = [result.id,result.title,result.overview, result.vote_average]
                    new_movie = searchresults(movie_details[0],movie_details[1],movie_details[2],movie_details[3])
                    db.session.add(new_movie)
                    db.session.commit()
                return render_template('moviedetails.html', searchresults=searchresults.query.all(), user_logged_in=1)
        
        #If user decides to clean their watchlist
        elif request.form['submit']=='Delete movie list':
            #Search if user watchlist has any movie
            exists=watchlist.query.filter_by(username=session['username']).first()
            if (exists):
                username=session['username']
                #Deleting all user movies from watchlist table
                db.session.query(watchlist).filter(watchlist.username==username).delete()
                db.session.commit()
                #Displaying updated watchlist
                return render_template('userlist.html' , watchlist=watchlist.query.filter(watchlist.username==session['username']))
            else:
                return render_template('userlist.html' , list_empty = 1 ,watchlist=watchlist.query.filter(watchlist.username==session['username']))

    else:
        return render_template('userlist.html' , watchlist=watchlist.query.filter(watchlist.username==session['username']))

#If user decides to search for a movie without logging into system
@app.route('/search', methods=['GET','POST'])
def search():
    if request.method == "POST":
        #First cleaning the searchlist table we use to store search resuts temporarily
        db.session.query(searchresults).delete()
        movie_name=request.form['movie_name']
        search= movie.search(movie_name)

        #If there are no search results
        if not search:
            return render_template('search.html', title = 'Movie Directory', no_result1=1)
        else:
            for result in search:
                #Temporarily saving returned movies into searchresults table and displaying it
                movie_details = [result.id,result.title,result.overview,result.vote_average]
                new_movie = searchresults(movie_details[0],movie_details[1],movie_details[2],movie_details[3])
                db.session.add(new_movie)
                db.session.commit()

            return render_template('moviedetails.html', searchresults=searchresults.query.all(), no_login=1)

    else:
        return render_template('search.html', title = 'Movie Directory')

@app.route('/changepassword', methods=['GET','POST'])
def changepassword():
    if request.method == "POST":
        username = request.form['Username']
        security_ans=request.form['secret_ans']
        new_pwd=generate_password_hash(request.form['new_pwd'])

        user_exists  = user_info.query.filter_by(username=username).first()
        if (user_exists):
            bool_ans=check_password_hash(user_exists.secret_ans,security_ans)
            #If user entered password matches with the encrypted password in db
            if bool_ans:
                user_exists.password=new_pwd
                db.session.commit()
                return render_template('changepassword.html',verified=1)
            else:
                return render_template('changepassword.html',verified=2)
        else:
            return render_template('changepassword.html',verified=2)
        
    return render_template('changepassword.html')

#if the user is logged and decides to remove a movie from the wathclist
@app.route('/removemovie', methods=['GET','POST'])
def removemovie():
    if request.method=="POST":
        movie_id=request.form['movie_id']
        not_new=watchlist.query.filter_by(movie_id=movie_id,username=session['username']).first()
	#if the movie was found on the wachtlist on the database for the given user
        if (not_new):
            if (not_new.username==session['username']):
                watchlist.query.filter_by(movie_id=movie_id, username=session['username']).delete()
                db.session.commit()
                return redirect(url_for('userlist'))

    return redirect(url_for('userlist'))

#if user wants to add a new movie to the watchlist
@app.route('/addmovie', methods=['GET','POST'])
def addmovie():
    if request.method=="POST":
        movie_id=request.form['movie_id']
        movie_name=request.form['movie_name']
        movie_score=request.form['movie_score']

        not_new=watchlist.query.filter_by(movie_id=movie_id, username=session['username']).first()
	#movie not found on watchlist case
        if (not_new):
            return render_template('moviedetails.html', searchresults=searchresults.query.all(), user_logged_in=1, not_added_movie=1)
	#movie succesfully found on user's watchlsit case
        else:
            new_movie = watchlist(session['username'],movie_id, movie_name,movie_score)
            try:
                db.session.add(new_movie)
                searchresults.query.filter_by(movie_id=movie_id, movie_score=movie_score).delete()
                db.session.commit()
                return render_template('moviedetails.html', searchresults=searchresults.query.all(), user_logged_in=1, added_movie=1)
            except:
                return render_template('moviedetails.html', searchresults=searchresults.query.all(), user_logged_in=1)
    return redirect(url_for('userlist'))

#logs the user out of the current session
@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    db.session.query(searchresults).delete()
    db.session.commit()
    session.pop('username', None)
    return redirect(url_for('index'))

#removes the user account of the databases
@app.route('/signout')
def signout():
    #Removing searchlist table
    db.session.query(searchresults).delete()
    db.session.commit()

    #Removing userinfo from userinfo table
    username=session['username']
    db.session.query(user_info).filter(user_info.username==username).delete()
    db.session.commit()

    #Removing movies of user from watchlist table
    db.session.query(watchlist).filter(watchlist.username==username).delete()
    db.session.commit()
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))

#! ERROR HANDLING: NONEXISTENT URL
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', title = '404'), 404

if __name__ == "__main__":
    #! LOCAL HOST
    db.create_all()
    ui.run()
    #app.run(debug = True, host = "127.0.0.1", port = "8000")
