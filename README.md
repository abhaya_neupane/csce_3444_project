# CSCE_3444_Project

**Movie List Web Application**

A flask framework based web application in Python. Unique encrypted login information for each user. After logging in users are provided with movie lits based on their previously mentioned favorites. Each user password and security answer are encrypted. Flaskwebgui is used to create a user friendly GUI. All user informations are recorded in a database using sqlalchemy as the tool. Bootstrap4, HTML and CSS implementations for properly setting up web interface. 

**How to run :** 
* Download webapp folder and open the folder in terminal or any IDE.
* Once inside the folder type "make start" in terminal or any IDE of choice. 
* Once all the requirements are downloaded if a user intends to use the program again simply type "make run".
* All system requirements are automatically downloaded and a virtual environment is created. 
* Program runs and a new window pops up which has movielist application.

**How to test :**
* Type "make test"
