from webapp import app
from webapp import user_info, watchlist, searchresults
import unittest

class FlaskTest(unittest.TestCase):

    def dbcreate(self):
        db.create_all()
        db.session.add(user_info("any", "test", "admin", "admin", "this is a test"))
        db.session.add(watchlist("admin", "1243433", "This will be a movie name", "9.3"))
        db.session.add(searchresults("1243233", "This would be a movie name", "This would be a movie overview", "9.3"))
        db.session.commit()

    def dbdelete(self):
        db.session.remove()
        db.drop_all()

#testing of the index page
    #testing the main page for a 200 response
    def test_index_response(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type = 'html/text')
        self.assertEqual(response.status_code, 200)

    #testing that the main page text is diplaying correctly
    def test_index_text(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type = 'html/text')
        self.assertTrue(b'MyMovieList is a personalized webapp that allows you to create a list of movies that you want to watch. Sign up now to create your own list, or if you already have a an account, log in to see your list.' in response.data)

    #testing that the sign up page can be displayed accordingly from main page
    def test_index_signup(self):
        tester = app.test_client(self)
        tester.get('/', content_type = 'html/text')
        response = tester.get('/newuser', follow_redirects=True)
        self.assertIn(b'New User details', response.data)

    #testing that the sign in page can be displayed accordingly from main page
    def test_index_signin(self):
        tester = app.test_client(self)
        tester.get('/', content_type = 'html/text')
        response = tester.get('/existinguser', follow_redirects=True)
        self.assertIn(b'Log In', response.data)

    #testing that the search page can be displayed accordingly from main page
    def test_index_search(self):
        tester = app.test_client(self)
        tester.get('/', content_type = 'html/text')
        response = tester.get('/search', follow_redirects=True)
        self.assertIn(b'Search Movies', response.data)
##
#testing of the log in page
     #testing the log in page for a 200 response
    def test_login_response(self):
        tester = app.test_client(self)
        response = tester.get('/existinguser', content_type = 'html/text')
        self.assertEqual(response.status_code, 200)

    #testing that the log in page text is diplaying correctly
    def test_login_text(self):
        tester = app.test_client(self)
        response = tester.get('/existinguser', content_type = 'html/text')
        self.assertTrue(b'Log In' in response.data)

    #testing that the log in page behaves correctly under correct credentials
    def test_login_correct(self):
        tester = app.test_client(self)
        tester.post('/newuser', data=dict(f_name ="this", l_name="is just", user_name = "a testing", password="application", password_again="application", secret_ans="try testing"), follow_redirects=True)
        response = tester.post('/existinguser', data = dict(Username="a testing", Password="application"), follow_redirects=True)
        self.assertIn(b'Your List', response.data)

    #testing that the log in page behaves correctly under incorrect credentials
    def test_login_incorrect(self):
        tester = app.test_client(self)
        response = tester.post('/existinguser', data = dict(Username="notadmin", Password="admin"), follow_redirects=True)
        self.assertIn(b'Incorrect credentials. Please try again...', response.data)

    #testing that the home page can be displayed accordingly from sign in
    def test_login_signup(self):
        tester = app.test_client(self)
        tester.get('/existinguser', content_type = 'html/text')
        response = tester.get('/', follow_redirects=True)
        self.assertIn(b'MyMovieList is a personalized webapp that allows you to create a list of movies that you want to watch. Sign up now to create your own list, or if you already have a an account, log in to see your list.', response.data)
##
    #testing that the change password can be accessed correctly from sign in page
    def test_login_changepassword(self):
        tester = app.test_client(self)
        tester.get('/existinguser', content_type = 'html/text')
        response = tester.get('/changepassword', follow_redirects=True)
        self.assertIn(b'Change Password', response.data)

#testing of the sing up page
    #testing the log in page for a 200 response
    def test_signup_response(self):
        tester = app.test_client(self)
        response = tester.get('/newuser', content_type = 'html/text')
        self.assertEqual(response.status_code, 200)

    #testing that the sing up page text is diplaying correctly
    def test_signup_text(self):
        tester = app.test_client(self)
        response = tester.get('/newuser', content_type = 'html/text')
        self.assertTrue(b'New User details' in response.data)

    #testing that the sign up page behaves correctly under correct input
    def test_signup_correct(self):
        tester = app.test_client(self)
        response = tester.post('/newuser', data = dict(f_name = "this", l_name = "is just", user_name = "a testing ", password = "application", password_again = "application", secret_ans = "try testing"), follow_redirects=True)
        self.assertIn(b'Log In', response.data)

    #testing that the sign up page behaves correctly under username taken
    def test_signup_user_taken(self):
        tester = app.test_client(self)
        response = tester.post('/newuser', data = dict(f_name = "this", l_name = "is just", user_name = "a testing ", password = "application", password_again = "application", secret_ans = "try testing"), follow_redirects=True)
        self.assertIn(b'Username taken. Please type unique username and try again...', response.data)
##
    #testing that the sign up page behaves correctly under no matching passwords
    def test_signup_nonmatch_pass(self):
        tester = app.test_client(self)
        response = tester.post('/newuser', data = dict(f_name = "this", l_name = "is just", user_name = "for a test", password = "application", password_again = "applicatio", secret_ans = "try testing"), follow_redirects=True)
        self.assertIn(b"Passwords didn't match.Please try again...", response.data)

#testing of the search function
    #testing movie search for successful search
    def test_search_response(self):
        tester = app.test_client(self)
        response = tester.get('/search', content_type = 'html/text')
        self.assertEqual(response.status_code, 200)

    #testing that a movie search is successfully added searchresults database
    def test_search_rightsearch(self):
        tester = app.test_client(self)
        response = tester.post('/search', data=dict(movie_name = 'Titanic'), follow_redirects = True)
        self.assertEqual(response.status_code, 200)
        movie = searchresults.query.filter_by(movie_name = 'Titanic').first()
        self.assertTrue(str(movie.movie_name) == 'Titanic')

#testing of the add a movie
    #movie correctly added case
    def test_addmovie_correct(self):
        tester = app.test_client(self)
        tester.post('/existinguser', data = dict(Username="admin", Password="admin"), follow_redirects=True)
        tester.post('/mylist', data=dict(movie_name = 'Titanic'), follow_redirects = True)
        response=tester.post('/addmovie', data=dict(movie_id="597", movie_name="Titanic", movie_score="7.9"), follow_redirects =True)
        self.assertEqual(response.status_code, 200)
        movie = watchlist.query.filter_by(movie_id = '597').first()
        self.assertTrue(str(movie.movie_name) == 'Titanic')

    #movie already on the list case
    def test_addmovie_again(self):
        tester = app.test_client(self)
        tester.post('/existinguser', data = dict(Username="admin", Password="admin"), follow_redirects=True)
        tester.post('/mylist', data=dict(movie_name = 'Titanic'), follow_redirects = True)
        tester.post('/addmovie', data=dict(movie_id="597", movie_name="Titanic", movie_score="7.9"), follow_redirects =True)
        response=tester.post('/addmovie', data=dict(movie_id="597", movie_name="Titanic", movie_score="7.9"), follow_redirects =True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Movie already exists in your list.',response.data)

#testing of remove a movie
    def test_remove_correct(self):
        tester = app.test_client(self)
        tester.post('/existinguser', data = dict(Username="admin", Password="admin"), follow_redirects=True)
        tester.post('/mylist', data=dict(movie_name = 'Titanic'), follow_redirects = True)
        response=tester.post('/removemovie', data=dict(movie_id="597"), follow_redirects =True)
        self.assertEqual(response.status_code, 200)
        movie = watchlist.query.filter_by(movie_id = '597').first()
        self.assertFalse(movie)


if __name__ == "__main__":
    unittest.main()
